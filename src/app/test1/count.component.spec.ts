import {CountComponent} from './count.component';
let component:CountComponent;
beforeEach(()=>{
    component = new CountComponent();
}) 
describe("counter",()=>{
    it('increment', () => {
        component.increment();
        expect(component.totalCount).toBe(1);
    })
    it('decrememt', () => {
        component.decrement();
        expect(component.totalCount).toBe(0);
    })
    
   
})