import {compute} from './compute'
describe('compute', () => {
    it('compute if', () => {
        const result = compute(-1);
        expect(result).toBe(0);
    })
    it('compute else', () => {
        const result = compute(1);
        expect(result).toBeGreaterThan(0);
    })
})