import {getCurrencies} from './getCurrencies'
describe('getCurrencies', () => {
    it('get Currencies', () => {
        const result = getCurrencies();
        expect(result).toContain(1);
        expect(result).toContain(2);
        expect(result).toContain(3);
    })
  
})