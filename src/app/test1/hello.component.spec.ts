let expected='';
let notexpected='';
beforeEach(()=>{
    expected ='hello';
    notexpected ='hello1';
})
afterAll(()=>{
    expected ='';
    notexpected ='';
})
describe('hello', () => {
    it('hello', () => {
        expect('hello').toBe('hello');
    })
    it('hello1', () => {
        expect('hello').not.toBe('hello1');
    })
})